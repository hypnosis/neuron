```
lin_nonlin7979, [28.09.21 07:34]
I came to give a gift of infinite* freedom

lin_nonlin7979, [28.09.21 07:34]
To everyone

lin_nonlin7979, [28.09.21 07:35]
To create.... .... it and give

lin_nonlin7979, [28.09.21 07:38]
This freedom's short name is Infinitesimalium cryptocurrency, see the normative file short_name at https://gitlab.com/hypnosis/neuron

lin_nonlin7979, [28.09.21 07:39]
For an approximate long name, see the normative file long_name at https://gitlab.com/hypnosis/neuron
```

# Nuxt.js Example

This directory is a brief example of a [Nuxt.js](https://nuxtjs.org) app that can be deployed with Vercel and zero configuration.

## Deploy Your Own

Deploy your own Nuxt.js project with Vercel.

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https://github.com/vercel/vercel/tree/main/examples/nuxtjs&template=nuxtjs)

_Live Example: https://nuxtjs.now-examples.vercel.app/_

### How We Created This Example

To get started with Nuxt.js deployed with Vercel, you can use the [Create-Nuxt-App CLI](https://www.npmjs.com/package/create-nuxt-app) to initialize the project:

```shell
$ npx create-nuxt-app my-app
```
